<?php

namespace WSIServices\Common\Validate;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
interface ValidateInterface {

	public function addPreProcess($function);

	public function setPreProcessContinueOnFail($continue = false);

	public function addPostProcess($function);

	public function setPostProcessContinueOnFail($continue = false);

	public function addAssertion($function, $errorString = null);

	public function setAssertionContinueOnFail($continue = false);

	public function addError($error);

	public function getErrors();

	public function clearErrors();

	public function setFailureFlag($assertion = true);

	public function getFailureFlag();

	public function walkPreProcess(&$data);

	public function walkAssertion($data);

	public function walkPostProcess(&$data);

	public function validate(&$data);

}