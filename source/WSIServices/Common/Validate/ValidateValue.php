<?php

namespace WSIServices\Common\Validate;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class ValidateValue implements ValidateInterface {

	/**
	 *
	 * @var string 
	 */
	protected $typeCast;

	/**
	 *
	 * @var array 
	 */
	protected $preProcess = array();

	/**
	 *
	 * @var boolean 
	 */
	protected $preProcessContinueOnFail = false;

	/**
	 *
	 * @var array 
	 */
	protected $postProcess = array();

	/**
	 *
	 * @var boolean 
	 */
	protected $postProcessContinueOnFail = false;

	/**
	 *
	 * @var array 
	 */
	protected $assertion = array();

	/**
	 *
	 * @var boolean 
	 */
	protected $assertionContinueOnFail = false;

	/**
	 *
	 * @var boolean 
	 */
	protected $failureFlag = false;

	/**
	 *
	 * @var array 
	 */
	protected $errors = array();

	/**
	 *
	 * @param callback $callback
	 * @throws \InvalidArgumentException 
	 */
	protected function throwNonCallable($callback) {
		if(!is_callable($callback))
			throw new \InvalidArgumentException('Provided function is not callable.');
	}

	/**
	 *
	 * @param string $type
	 * @return ValidateValue
	 * @throws \InvalidArgumentException 
	 */
	public function setTypeCast($type) {
		$test = null;
		if(!settype($test, $type))
			throw new \InvalidArgumentException('Provided type is not valid.');
		$this->typeCast = $type;
		return $this;
	}

	/**
	 *
	 * @return string 
	 */
	public function getTypeCast() {
		return $this->typeCast;
	}

	/**
	 *
	 * @return ValidateValue 
	 */
	public function clearTypeCast() {
		$this->typeCast = null;
		return $this;
	}

	/**
	 *
	 * @param callback $function
	 * @return ValidateValue 
	 */
	public function addPreProcess($function) {
		$this->throwNonCallable($function);
		$this->preProcess[] = $function;
		return $this;
	}

	/**
	 *
	 * @return array 
	 */
	public function getPreProcess() {
		return $this->preProcess;
	}

	/**
	 *
	 * @return integer 
	 */
	public function countPreProcess() {
		return count($this->preProcess);
	}

	/**
	 *
	 * @param null|integer|string $index
	 * @return ValidateValue 
	 */
	public function clearPreProcess($index = null) {
		if($index === null) {
			$this->preProcess = array();
		} else {
			unset($this->preProcess[$index]);
		}
		return $this;
	}

	/**
	 *
	 * @param boolean $continue
	 * @return ValidateValue 
	 */
	public function setPreProcessContinueOnFail($continue = false) {
		$this->preProcessContinueOnFail = $continue;
		return $this;
	}

	/**
	 *
	 * @param callback $function
	 * @return ValidateValue 
	 */
	public function addPostProcess($function) {
		$this->throwNonCallable($function);
		$this->postProcess[] = $function;
		return $this;
	}

	/**
	 *
	 * @return array 
	 */
	public function getPostProcess() {
		return $this->postProcess;
	}

	/**
	 *
	 * @return integer 
	 */
	public function countPostProcess() {
		return count($this->postProcess);
	}

	/**
	 *
	 * @param null|integer|string $index
	 * @return ValidateValue 
	 */
	public function clearPostProcess($index = null) {
		if($index === null) {
			$this->postProcess = array();
		} else {
			unset($this->postProcess[$index]);
		}
		return $this;
	}

	/**
	 *
	 * @param boolean $continue
	 * @return ValidateValue 
	 */
	public function setPostProcessContinueOnFail($continue = false) {
		$this->postProcessContinueOnFail = $continue;
		return $this;
	}

	/**
	 *
	 * @param callback $function
	 * @param null|string $errorString
	 * @return ValidateValue 
	 */
	public function addAssertion($function, $errorString = null) {
		$this->throwNonCallable($function);
		$this->assertion[] = array(
			'f' => $function,
			'e' => $errorString,
		);
		return $this;
	}

	/**
	 *
	 * @return array 
	 */
	public function getAssertion() {
		return $this->assertion;
	}

	/**
	 *
	 * @return integer 
	 */
	public function countAssertion() {
		return count($this->assertion);
	}

	/**
	 *
	 * @param null|integer|string $index
	 * @return ValidateValue 
	 */
	public function clearAssertion($index = null) {
		if($index === null) {
			$this->assertion = array();
		} else {
			unset($this->assertion[$index]);
		}
		return $this;
	}

	/**
	 *
	 * @param boolean $continue
	 * @return ValidateValue 
	 */
	public function setAssertionContinueOnFail($continue = false) {
		$this->assertionContinueOnFail = $continue;
		return $this;
	}

	/**
	 *
	 * @param string $error
	 * @return ValidateValue 
	 */
	public function addError($error) {
		$this->errors[] = $error;
		return $this;
	}

	/**
	 *
	 * @return array 
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 *
	 * @return ValidateValue 
	 */
	public function clearErrors() {
		$this->errors = array();
		return $this;
	}

	/**
	 *
	 * @param boolean $assertion
	 * @return ValidateValue 
	 */
	public function setFailureFlag($assertion = true) {
		$this->failureFlag = $assertion;
		return $this;
	}

	/**
	 *
	 * @return boolean 
	 */
	public function getFailureFlag() {
		return $this->failureFlag;
	}

	/**
	 *
	 * @param mixed $data
	 * @return boolean 
	 */
	public function typeCast(&$data) {
		if($this->typeCast === null) return true;
		return settype($data, $this->typeCast);
	}

	/**
	 *
	 * @param mixed $data
	 * @return boolean 
	 */
	public function walkPreProcess(&$data) {
		$this->failureFlag = false;
		foreach($this->preProcess as $process) {
			call_user_func($process, $data, $this);
			if($this->failureFlag) {
				if(!$this->preProcessContinueOnFail)
					return false;
				$this->failureFlag = false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param mixed $data
	 * @return boolean 
	 */
	public function walkAssertion($data) {
		$this->failureFlag = false;
		foreach($this->assertion as $process) {
			call_user_func($process['f'], $data, $this);
			if($this->failureFlag) {
				$this->failureFlag = false;
				if(is_string($process['e']) && $process['e'] !== '')
					$this->addError($process['e']);
				if(!$this->assertionContinueOnFail)
					return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param mixed $data
	 * @return boolean 
	 */
	public function walkPostProcess(&$data) {
		$this->failureFlag = false;
		foreach($this->postProcess as $process) {
			call_user_func($process, $data, $this);
			if($this->failureFlag) {
				if(!$this->postProcessContinueOnFail)
					return false;
				$this->failureFlag = false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param mixed $data
	 * @return boolean 
	 */
	public function validate(&$data) {
		$this->typeCast($data);

		if(!$this->walkPreProcess($data) && !$this->preProcessContinueOnFail)
			return false;

		if(!$this->walkAssertion($data) && !$this->assertionContinueOnFail)
			return false;

		if(!$this->walkPostProcess($data) && !$this->postProcessContinueOnFail)
			return false;

		return true;
	}

}
