<?php

namespace WSIServices\Common\Validate;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class ValidateArray extends ValidateValue {

	protected $elementClass = '\WSIServices\Common\Validate\ValidateValue';
	protected $elements = array();

	protected $elementContinueOnFail = false;

	public function setElementClass($class) {
		if(!class_exists($class))
			throw new \InvalidArgumentException('Class `'.$class.'` can not be found.');
		$this->elementClass = $class;
		return $this;
	}

	public function getElementClass() {
		return $this->elementClass;
	}

	public function issetElement($key) {
		return array_key_exists($key, $this->elements);
	}

	public function setElement($key, $element) {
		if($this->issetElement($key))
			throw new \InvalidArgumentException('Element `'.$key.'` already exists.');
		return $this->elements[$key] = $element;
	}

	public function newElement($key) {
		return $this->setElement($key, new $this->elementClass);
	}

	public function getElement($key) {
		if(!$this->issetElement($key))
			throw new \InvalidArgumentException('Element `'.$key.'` does not exist.');
		return $this->elements[$key];
	}

	public function existsElement($element) {
		$exists = array();
		foreach($this->elements as $elementName => $elementValue) {
			if($elementValue === $element) $exists[] = $elementName;
		}
		return ($exists ? $exists : false);
	}

	public function removeElement($key) {
		if(!$this->issetElement($key))
			throw new \InvalidArgumentException('Element `'.$key.'` does not exist.');
		unset($this->elements[$key]);
		return $this;
	}

	public function setElementContinueOnFail($continue = false) {
		$this->elementContinueOnFail = $continue;
		return $this;
	}

	public function validate(&$array) {
		foreach($array as $elementName => $elementValue) {
			if(array_key_exists($elementName, $this->elements)) {
				$element = $this->elements[$elementName];
				$element->validate($elementValue);

				$errors = $element->getErrors();
				if(count($errors))
					foreach($errors as $error)
						$this->addError($error);
			}
		}
	}

}