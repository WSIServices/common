<?php

namespace WSIServices\Common\Nodes;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class NodeParent extends NodeChild {

	/**
	 * @var string Name of class
	 */
	private $nodeChildType = __CLASS__;

	/**
	 * Set name for section and supply a section array & parent
	 * @param string $name [optional]
	 * @param array $section [optional]
	 * @param type $parent 
	 */
	public function __construct(&$section = null, $name = null, &$parent = null) {
		parent::__construct($section, $name, $parent);
	}

	/**
	 * Internal check of section 
	 */
	protected function nodeCheckSection() {
		parent::nodeCheckSection();

		if(key_exists('_', $this->nodeSection) && key_exists('childType', $this->nodeSection['_'])) {
			$this->nodeChildType =& $this->nodeSection['_']['childType'];
		} else {
			$this->nodeSection['_']['childType'] =& $this->nodeChildType;
		}
	}

	/**
	 * 
	 * @param string $name Name of node to add
	 * @param string $class Class to use a node
	 * @return object Created Node
	 */
	public function nodeAdd($name, $class = null) {
		if(!key_exists($name, $this->nodeSection))
				$this->nodeSection[$name] = array();

		if($class === null) $class = $this->nodeChildType;

		return new $class($name, $this->nodeSection[$name], $this);
	}

	/**
	 *
	 * @param string $name
	 * @return boolean
	 * @throws \InvalidArgumentException 
	 */
	public function &node($name) {
		if(is_string($name)) $name = array_filter(explode('\\', $name));
		if(!is_array($name)) throw new \InvalidArgumentException('Node name not provided as string or array type');
		if(!count($name)) return false;

		$path = array();
		$section =& $this->nodeSection;
		foreach($name as $pos => $key) {
			if(!key_exists($key, $section)) throw new \InvalidArgumentException('Node name not valid');

			$path[$pos] =& $section[$name[$pos]];
			unset($section); $section =& $path[$pos];
		}

		if(key_exists('_', $section) &&
			key_exists('self', $section['_']) &&
			is_object($section['_']['self'])) {
				return $section['_']['self'];
		} else {
			return $section;
		}
	}

}