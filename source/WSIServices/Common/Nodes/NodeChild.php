<?php

namespace WSIServices\Common\Nodes;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class NodeChild extends Node {

	/**
	 * @var string refferance to parent
	 */
	protected $nodeParent;

	/**
	 * Set name for section and supply a section array & parent
	 * @param array $section [optional]
	 * @param string $name [optional]
	 * @param type $parent [optional]
	 */
	public function __construct(&$section = null, $name = null, &$parent = null) {
		parent::__construct($section, $name);
		if($parent !== null) $this->nodeSetParent($parent);
	}

	/**
	 * Internal check of section 
	 */
	protected function nodeCheckSection() {
		parent::nodeCheckSection();

		if(!key_exists('parent', $this->nodeSection['_'])) {
			$this->nodeSection['_']['parent'] =& $this->nodeParent;
		} else {
			$this->nodeParent =& $this->nodeSection['_']['parent'];
		}
	}

	/**
	 *
	 * @param object $parent
	 * @throws \InvalidArgumentException 
	 */
	public function nodeSetParent(&$parent) {
		if(!is_object($parent)) throw new \InvalidArgumentException('Provided parent must be an object');
		if(!method_exists($parent, 'nodeGetName')) throw new \InvalidArgumentException('Provided parent must implement method nodeGetName');
		$this->nodeParent = $parent;
	}

	/**
	 * Retrieve nodes parent node
	 * @return object Node parent
	 */
	public function &nodeGetParent() {
		return $this->nodeParent;
	}

	/**
	 * Return name of node including slash seperated path
	 * @param boolean $path [optional]
	 * @return string 
	 */
	public function nodeGetName($path = false) {
		if($path !== true) return $this->nodeName;

		$return = $this->nodeGetPath();
		return ($return ? $return.'\\' : '').$this->nodeName;
	}

	/**
	 * Return slash seperated path to node
	 * @return string 
	 */
	public function nodeGetPath() {
		if(!$this->nodeParent) return '';

		try {
			$return = $this->nodeParent->nodeGetName(true);
		} catch (\BadMethodCallException $e) {
			$return = '';
		}

		return $return;
	}

}