<?php

namespace WSIServices\Common\Nodes;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class Node implements \ArrayAccess, \Countable, \IteratorAggregate, \Serializable {

	/**
	 * @var array Node section
	 */
	protected $nodeSection = array();

	/**
	 * @var string Node name
	 */
	protected $nodeName = '';

	/**
	 * @var array Node callback array
	 */
	protected $nodeCallback = array();

	/**
	 * Set node section
	 * @param array $section [optional]
	 * @param string $name [optional]
	 */
	public function __construct(&$section = null, $name = null) {
		$this->nodeSetSection($section);
		if($name !== null) $this->nodeSetName($name);
	}

	/**
	 * Internal check of section 
	 */
	protected function nodeCheckSection() {
		if(!is_array($this->nodeSection)) $this->nodeSection = array('_' => array());
		elseif(!key_exists('_', $this->nodeSection)) $this->nodeSection['_'] = array();

		$this->nodeSection['_']['type'] = get_class($this);
		$this->nodeSection['_']['self'] =& $this;

		if(!key_exists('name', $this->nodeSection['_'])) {
			$this->nodeSection['_']['name'] =& $this->nodeName;
		} else {
			$this->nodeName =& $this->nodeSection['_']['name'];
		}

		if(!key_exists('callback', $this->nodeSection['_'])) {
			$this->nodeSection['_']['callback'] =& $this->nodeCallback;
		} else {
			$this->nodeCallback =& $this->nodeSection['_']['callback'];
		}
	}

	/**
	 * Set node section
	 * @param array $section
	 */
	public function nodeSetSection(&$section) {
		$this->nodeSection =& $section;
		$this->nodeCheckSection();
	}

	/**
	 * Get node section
	 * @return array
	 */
	public function &nodeGetSection() {
		return $this->nodeSection;
	}

	/**
	 * Set node name
	 * @param array $section
	 */
	public function nodeSetName($name) {
		$this->nodeName = (string) $name;
	}

	/**
	 * Get node name
	 * @return array
	 */
	public function nodeGetName() {
		return (string) $this->nodeName;
	}

	/**
	 * Determin if specified callback in node exists
	 * @param string $name
	 * @return boolean
	 */
	public function nodeCallbackExists($name) {
		return key_exists($name, $this->nodeCallback);
	}

	/**
	 * Set specified callback in node
	 * @param string $name
	 * @param closure $callable
	 * @param boolean $overwrite [optional]
	 */
	public function nodeCallbackSet($name, &$callable, $overwrite = false) {
		if((!key_exists($name, $this->nodeCallback) || $overwrite) && is_callable($callable))
			$this->nodeCallback[$name] =& $callable;
	}

	/**
	 * Unset specified call in node
	 * @param string $name 
	 */
	public function nodeCallbackUnset($name) {
		if(key_exists($name, $this->nodeCallback)) unset($this->nodeCallback[$name]);
	}

	/**
	 * Call specified Closure in node
	 * @param string $name
	 * @param array $arguments
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		if(!key_exists($name, $this->nodeCallback))
			throw new \BadMethodCallException("Invalid node method callback name '$name'");
		return call_user_func_array($this->nodeCallback[$name], $arguments);
	}

	/**
	 * Return string representaion of this node 
	 * @return string
	 */
	public function __toString() {
		$section = $this->nodeSection;
		unset($section['_']['self']);
		return var_export($section, true);
	}

	/**
	 * Determin if specified item in node is set
	 * @param string $offset
	 * @return boolean
	 */
	public function __isset($name) {
		return isset($this->nodeSection[$name]);
	}

	/**
	 * Return specified item in node
	 * @param string $offset
	 * @return mixed 
	 */
	public function &__get($name) {
		return $this->nodeSection[$name];
	}

	/**
	 * Set specified item in node
	 * @param string $offset
	 * @param mixed $value
	 */
	public function __set($name, $value) {
		$this->nodeSection[$name] = $value;
	}

	/**
	 * Unset specified item in node
	 * @param string $offset
	 */
	public function __unset($name) {
		unset($this->nodeSection[$name]);
	}

	/**
	 * Determin if specified item in node exists
	 * @param mixed $offset
	 * @return boolean
	 */
	public function offsetExists($offset) {
		return isset($this->nodeSection[$offset]);
	}

	/**
	 * Return specified item in node
	 * @param mixed $offset
	 * @return mixed
	 */
	public function offsetGet($offset) {
		return $this->nodeSection[$offset];
	}

	/**
	 * Set specified item in node
	 * @param mixed $offset
	 * @param mixed $value
	 */
	public function offsetSet($offset, $value) {
		if(is_null($offset)) $this->nodeSection[] = $value;
		else $this->nodeSection[$offset] = $value;
	}

	/**
	 * Unset specified item in node
	 * @param mixed $offset
	 */
	public function offsetUnset($offset) {
		unset($this->nodeSection[$offset]);
	}

	/**
	 * Return count of items in node
	 * @return integer
	 */
	public function count() {
		return count($this->nodeSection) - 1;
	}

	/**
	 * Retrun an iterator of this node
	 * @return \ArrayIterator 
	 */
	public function getIterator() {
		$return  = new \ArrayIterator($this->nodeSection);
		unset($return['_']);
		return $return;
	}

	/**
	 * Condense node to string
	 * @return string
	 */
	public function serialize() {
		$section = $this->nodeSection;
		unset($section['_']['self']);
		$section['_']['callback'] = array();
		return serialize($section);
	}

	/**
	 * Reconstitute node from string
	 * @param string $data 
	 */
	public function unserialize($data) {
		$this->nodeSection = unserialize($data);
		$this->nodeCheckSection();
	}

}