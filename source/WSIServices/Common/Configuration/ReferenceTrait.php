<?php

namespace WSIServices\Common\Configuration;

/**
 * @package WSI-Services Common Configuration
 * @author Sam Likins
 * @copyright Copyright (c) 2013, WSI-Services
 * @link http://wsi-services.com
 *
 * @example
 *
 *	use WSIService\Common;
 *
 *	class ConfigurationContainer {
 *		use Configuration\StrictTrait,
 *			Configuration\ReferenceTrait;
 *
 *		public $publicProperty = 'value1';
 *		protected $protectedProperty = 'value2';
 *		private $privateProperty = 'value3';
 *
 *		public function get_object_vars() {
 *			return get_object_vars($this);
 *		}
 *	}
 * 
 *	$cC = new ConfigurationContainer();
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
trait ReferenceTrait {

	/**
	 * Tie Object Paramiters to provided configuration array
	 * @param array $configuration [optional]
	 *
	 * The configuration paramiter is passed by referance, so it must be a variable.
	 *
	 * @example Providing Empty Variable
	 *	$cC->SetConfiguration($configuration);
	 *
	 * @example Providing Array
	 *	$configuration = array(
	 *		'PublicProperty1' => 'works',
	 *		'ProtectedProperty1' => 'works',
	 *		'PrivateProperty1' => 'works',
	 *		'nonExist' => false,
	 *	);
	 *	$cC->setConfiguration($configuration);
	 *
	 */
	public function setConfiguration(&$configuration = null) {
		if(!is_array($configuration))
			$configuration = array();

		// Instantiated objects paramiters
		$classVariables = get_object_vars($this);

		// Get class paramiters that are not configuration keys
		$missingParams = array_diff_key($classVariables, $configuration);

		// Create configuration keys and store class paramiter values
		foreach($missingParams as $classParamiter => $paramiterValue)
			$configuration[$classParamiter] = $paramiterValue;

		$this->processInternalConfiguration($configuration, $classVariables);
	}

}