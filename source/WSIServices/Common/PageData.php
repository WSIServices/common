<?php

namespace WSIServices\Common;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class PageData extends ArrayClass {

	protected $container = array();

	protected $content = array();

	protected $ob_section;

	protected $values = array();

	protected $headers = array(
		'title' => '',
		'description' => '',
		'keywords' => array(),
		'css' => array(),
		'cssLink' => array(),
		'javascript' => array(),
		'javascriptLink' => array(),
	);

	public $replaceStrings = array(
		'css' => "<style type=\"text/css\">\n[css]\n</style>",
		'cssLink' => '<link rel="stylesheet" type="text/css" media="[media]" href="[href]" />',
		'javascript' => "<script type=\"text/javascript\" language=\"javascript\">\n[javascript]\n</script>",
		'javascriptLink' => '<script type="text/javascript" language="javascript" src="[src]"></script>',
	);

	public $section = 0;

	/**
	 *
	 * @param mixed $container
	 * @return \WSIServices\Common\PageData
	 */
	public function setContainer(&$container) {
		$this->container =& $container;
		return $this;
	}

	/**
	 *
	 * @return mixed
	 */
	public function &getContainer() {
		return $this->container;
	}

	/**
	 *
	 * @param string $name
	 * @param mixed $value 
	 * @return \WSIServices\Common\PageData
	 */
	public function setValue($name, $value) {
		$this->values[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return boolean 
	 */
	public function issetValue($name) {
		return isset($this->values[$name]);
	}

	/**
	 *
	 * @param string $name
	 * @return mixed 
	 */
	public function getValue($name) {
		if(isset($this->values[$name]))
			return $this->values[$name];
	}

	/**
	 *
	 * @param string $name 
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetValue($name) {
		if(isset($this->values[$name]))
			unset($this->values[$name]);
		return $this;
	}

	/**
	 *
	 * @param string $header
	 * @param mixed $value 
	 * @return \WSIServices\Common\PageData
	 */
	public function setHeader($header, $value) {
		$this->headers[$header] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $header
	 * @return boolean
	 */
	public function issetHeader($header) {
		return isset($this->headers[$header]);
	}

	/**
	 *
	 * @param string $header
	 * @return mixed
	 */
	public function getHeader($header) {
		if(isset($this->headers[$header]))
			return $this->headers[$header];
	}

	/**
	 *
	 * @param string $header 
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetHeader($header) {
		if(isset($this->headers[$header]))
			unset($this->headers[$header]);
		return $this;
	}

	/**
	 *
	 * @param string $data
	 * @param string $name [optional]
	 * @return \WSIServices\Common\PageData
	 */
	public function setCss($data, $name = null) {
		if($name === null)
			$this->headers['css'][] = $data;
		else
			$this->headers['css'][$name] = $data;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function issetCss($name = null) {
		if($name === null)
			return count($this->headers['css']) > 0;
		else
			return isset($this->headers['css'][$name]);
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getCss($name = null) {
		if($name === null)
			return implode(PHP_EOL, $this->headers['css']);
		elseif(!isset($this->headers['css'][$name]))
			return;
		else
			return $this->headers['css'][$name];
	}

	/**
	 *
	 * @param string $name 
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetCss($name) {
		if(isset($this->headers['css'][$name]))
			unset($this->headers['css'][$name]);
		return $this;
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getCssString($name = null) {
		$css = $this->getCss($name);

		if($css)
			return str_replace('[css]', $css, $this->replaceStrings['css']);
	}

	/**
	 *
	 * @param string $href url of the CSS file
	 * @param string $media [optional] CSS media type
	 * @param string $name [optional]
	 * @return \WSIServices\Common\PageData
	 */
	public function setCssLink($href, $media = 'all', $name = null) {
		if($name === null)
			$this->headers['cssLink'][] = array(
				'href' => $href,
				'media' => $media,
			);
		else
			$this->headers['cssLink'][$name] = array(
				'href' => $href,
				'media' => $media,
			);
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function issetCssLink($name = null) {
		if($name === null)
			return count($this->headers['cssLink']) > 0;
		else
			return isset($this->headers['cssLink'][$name]);
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return array
	 */
	public function getCssLink($name = null) {
		if($name === null)
			return $this->headers['cssLink'];
		elseif(!isset($this->headers['cssLink'][$name]))
			return;
		else
			return array($this->headers['cssLink'][$name]);
	}

	/**
	 *
	 * @param string $name
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetCssLink($name) {
		if(isset($this->headers['cssLink'][$name]))
			unset($this->headers['cssLink'][$name]);
		return $this;
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getCssLinkString($name = null) {
		$links = $this->getCssLink($name);

		$return = array();
		foreach($links as $link) {
			$return[] = str_replace(
				array('[media]','[href]'),
				array($link['media'], $link['href']),
				$this->replaceStrings['cssLink']
			);
		}

		if(count($return))
			return implode(PHP_EOL, $return);
	}

	/**
	 *
	 * @param string $data
	 * @param string $name [optional]
	 * @return \WSIServices\Common\PageData
	 */
	public function setJavascript($data, $name = null) {
		if($name === null)
			$this->headers['javascript'][] = $data;
		else
			$this->headers['javascript'][$name] = $data;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function issetJavascript($name = null) {
		if($name === null)
			return count($this->headers['javascript']) > 0;
		else
			return isset($this->headers['javascript'][$name]);
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getJavascript($name = null) {
		if($name === null)
			return implode(PHP_EOL, $this->headers['javascript']);
		elseif(!isset($this->headers['javascript'][$name]))
			return;
		else
			return $this->headers['javascript'][$name];
	}

	/**
	 *
	 * @param string $name 
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetJavascript($name) {
		if(isset($this->headers['javascript'][$name]))
			unset($this->headers['javascript'][$name]);
		return $this;
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getJavasctiptString($name = null) {
		$javascript = $this->getJavascript($name);

		if($javascript)
			return str_replace('[javascript]', $javascript, $this->replaceStrings['javascript']);
	}

	/**
	 *
	 * @param string $src url of the JavaScript file
	 * @param string $name [optional]
	 * @return \WSIServices\Common\PageData
	 */
	public function setJavascriptLink($src, $name = null) {
		if($name === null)
			$this->headers['javascriptLink'][] = $src;
		else
			$this->headers['javascriptLink'][$name] = $src;
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function issetJavascriptLink($name = null) {
		if($name === null)
			return count($this->headers['javascriptLink']) > 0;
		else
			return isset($this->headers['javascriptLink'][$name]);
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return array
	 */
	public function getJavascriptLink($name = null) {
		if($name === null)
			return $this->headers['javascriptLink'];
		elseif(!isset($this->headers['javascriptLink'][$name]))
			return;
		else
			return array($this->headers['javascriptLink'][$name]);
	}

	/**
	 *
	 * @param string $name 
	 * @return \WSIServices\Common\PageData
	 */
	public function unsetJavascriptLink($name) {
		if(isset($this->headers['javascriptLink'][$name]))
			unset($this->headers['javascriptLink'][$name]);
		return $this;
	}

	/**
	 *
	 * @param string $name [optional]
	 * @return string
	 */
	public function getJavascriptLinkString($name = null) {
		$links = $this->getJavascriptLink($name);

		$return = array();
		foreach($links as $link) {
			$return[] = str_replace('[src]', $link, $this->replaceStrings['javascriptLink']);
		}

		if(count($return))
			return implode(PHP_EOL, $return);
	}

	/**
	 *
	 * @param mixed $data Content to add to current content in section
	 * @param string $section [optional] Integer or string of the name of the section
	 * @return \WSIServices\Common\PageData
	 */
	public function setContent($data, $section = null) {
		if($section === null) $section = $this->section;
		if(!isset($this->content[$section]))
			$this->content[$section] = '';

		$this->content[$section] .= $data;
		return $this;
	}

	/**
	 *
	 * @param string $section Integer or string of the name of the section
	 * @return boolean
	 */
	public function issetContent($section) {
		return isset($this->content[$section]);
	}

	/**
	 *
	 * @param string $section [optional] Integer or string of the name of the section
	 * @return string
	 */
	public function getContent($section = null) {
		if($section === null) $section = $this->section;

		if(isset($this->content[$section]))
			return $this->content[$section];
	}

	/**
	 *
	 * @param string $section [optional] Integer or string of the name of the section
	 * @return \WSIServices\Common\PageData
	 */
	public function clearContent($section = null) {
		if($section === null) $section = $this->section;

		if(isset($this->content[$section]))
			unset($this->content[$section]);
		return $this;
	}

	/**
	 *
	 * @param string $section [optional] Integer or string of the name of the section
	 * @param boolean $flush [optional] Send content from OB to current section if present
	 * @return \WSIServices\Common\PageData
	 */
	public function setContentOB($section = null, $flush = true) {
		if($section === null) $section = $this->section;
		if(!is_null($this->ob_section) && $flush)
			$this->endContentOB();

		$this->ob_section = $section;
		ob_start();
		return $this;
	}

	/**
	 *
	 * @param string $section [optional] Integer or string of the name of the section
	 * @return \WSIServices\Common\PageData
	 */
	public function endContentOB($section = null) {
		if(!is_null($section)) $this->ob_section = $section;
		if(!is_null($this->ob_section) && ob_get_length()) {
			if(!isset($this->content[$this->ob_section]))
				$this->content[$this->ob_section] = '';
			$this->content[$this->ob_section] .= ob_get_contents();
		}

		$this->ob_section = null;
		ob_end_clean();
		return $this;
	}

}