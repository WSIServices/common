<?php

namespace WSIServices\Common;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Create Object:
 *
 *	$configuration = array(
 *		'directory' => '/path/to/directory',
 *	);
 *	$dirIncluder = new DirectoryIncluder($configuration);
 *
 */
class DirectoryIncluder extends ArrayClass {

	public $directory;
	public $extension = '.php';

	/**
	 * Provide configuration
	 * @param mixed $configuration [optional]
	 *
	 * @example Providing No Variable
	 *	$dI = new DirectoryIncluder();
	 *
	 * @example Providing Empty Variable
	 *	$dI = new DirectoryIncluder($configuration);
	 *
	 * @example Providing Empty Variable
	 *	$configuration = __DIR__;
	 *	$dI = new DirectoryIncluder($configuration);
	 *
	 * @example Providing Array
	 *	$configuration = array(
	 *		'directory' => __DIR__,
	 *		'extension' => '.io.php',
	 *	);
	 *	$dI = new DirectoryIncluder($configuration);
	 *
	 */
	public function __construct(&$configuration = null) {
		if(is_string($configuration)) $configuration = array(
			'directory' => $configuration,
		);

		parent::__construct($configuration);
	}

	/**
	 * Check if directory exists
	 * @throws \UnexpectedValueException If directory does not exist
	 */
	protected function checkDirectory() {
		if(!file_exists($this->directory))
			throw new \UnexpectedValueException("Provided directory path '$this->directory' does not exist");
	}

	/**
	 * Get path of file
	 * @param string $name Name of file to return path
	 * @return string Path to specified file
	 *
	 * @example
	 * $path = $dI->path('filename');
	 */
	public function path($name) {
		$this->checkDirectory();
		$name = $this->directory.\DIRECTORY_SEPARATOR.$name;
		if(is_readable($name.$this->extension)) $name .=$this->extension;
		return realpath($name);
	}

	/**
	 * Check if file is loadable
	 * @param string $name Name of file to check
	 * @param boolean $returnPath Return path of file if loadable
	 * @return mixed If $returnPath is true, return string of file path, else return boolean
	 *
	 * @example Basic usage
	 * $return = $dI->loadable('filename');
	 *
	 * @example Return Path
	 * $path = $dI->loadable('filename', true);
	 */
	public function loadable($name, $returnPath = false) {
		$path = $this->path($name);
		return is_readable($path) ? ($returnPath ? $path : true) : false;
	}

	/**
	 * Load specified file
	 * @param string $name Name of file
	 * @return mixed return value of file include
	 * @throws \UnexpectedValueException 
	 *
	 * @example
	 * $return = $dI->load('filename');
	 */
	public function load($name) {
		$path = $this->loadable($name, true);
		if($path === false) throw new \UnexpectedValueException("Provided filename '$name' is not readable, with extension '$this->extension' at location '$this->directory'");
		return include $path;
	}

}