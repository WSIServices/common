<?php

namespace WSIServices\Common;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * ArrayClass must be extended:
 *
 * @example
 *	class ArrayClassContainer extends ArrayClass {
 *		public $publicProperty = 'value1';
 *		protected $protectedProperty = 'value2';
 *		private $privateProperty = 'value3';
 *
 *		public function get_object_vars() {
 *			return get_object_vars($this);
 *		}
 *	}
 * 
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
abstract class ArrayClass {

	use Configuration\ReferenceTrait,
		Configuration\StrictTrait;

	/**
	 * Tie Module Paramiters to provided configuration array
	 * @param array $configuration [optional]
	 *
	 * ArrayClass can be instanciated with or without a configuration paramiter.
	 * The configuration paramiter is passed by referance, so it must be a variable.
	 *
	 * @example Providing No Variable
	 *	$aCC = new ArrayClassContainer();
	 *
	 * @example Providing Empty Variable
	 *	$aCC = new ArrayClassContainer($configuration);
	 *
	 * @example Providing Array
	 *	$configuration = array(
	 *		'PublicProperty1' => 'works',
	 *		'ProtectedProperty1' => 'works',
	 *		'PrivateProperty1' => 'works',
	 *		'nonExist' => false,
	 *	);
	 *	$aCC = new ArrayClassContainer($configuration);
	 *
	 */
	public function __construct(&$configuration = null) {
		if(func_num_args())
			$this->setConfiguration($configuration);
	}

}