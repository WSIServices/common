<?php

namespace WSIServices\Common;

/**
 * @package WSI-Services Common
 * @author Sam Likins
 * @copyright Copyright (c) 2012, WSI-Services
 * @link http://wsi-services.com
 *
 * @license http://opensource.org/licenses/gpl-3.0.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
class ReplaceMap extends ArrayClass {

	protected $brackets = array(
		'pre' => '<replaceMap name="',
		'post' => '" />',
	);

	protected $map;

	public $addOverwrite = false;

	/**
	 *
	 * @param string $pre 
	 * @param string $post 
	 * @return \WSIServices\Common\ReplaceMap 
	 */
	public function setBrackets($pre, $post) {
		$this->brackets['pre'] = $pre;
		$this->brackets['post'] = $post;
		return $this;
	}

	/**
	 *
	 * @param string $needle 
	 * @return \WSIServices\Common\ReplaceMap 
	 */
	public function setOverwrite($overwrite) {
		$this->addOverwrite = (boolean) $overwrite;
		return $this;
	}

	/**
	 *
	 * @param string $needle 
	 * @param string $replace 
	 * @param boolean $overwrite 
	 * @return \WSIServices\Common\ReplaceMap 
	 * @throws OutOfBoundsException 
	 */
	public function add($needle, $replace, $overwrite = null) {
		if(is_null($overwrite)) $overwrite = (bool) $this->addOverwrite;
		if($this->exist($needle) && !$overwrite)
			throw new \OutOfBoundsException('Needle `'.$needle.'` is already mapped, must specify overwrite.');

		$this->map[$needle] = $replace;
		return $this;
	}

	/**
	 *
	 * @param string $needle 
	 * @return \WSIServices\Common\ReplaceMap 
	 * @throws OutOfBoundsException 
	 */
	public function remove($needle) {
		if(!$this->exist($needle))
			throw new \OutOfBoundsException('Needle `'.$needle.'` is not mapped.');
		unset($this->map[$needle]);
		return $this;
	}

	/**
	 *
	 * @param string $needle 
	 * @return boolean 
	 */
	public function exist($needle) {
		return isset($this->map[$needle]);
	}

	/**
	 *
	 * @param string $needle 
	 * @return string 
	 * @throws OutOfBoundsException 
	 */
	public function retrieve($needle) {
		if(!isset($this->map[$needle]))
			throw new \OutOfBoundsException('Needle `'.$needle.'` is not mapped.');
		return $this->map[$needle];
	}

	/**
	 *
	 * @param string $subject 
	 * @return string 
	 */
	public function replace($subject) {
		foreach ($this->map as $needle => $replace) {
			$needle = $this->brackets['pre'].$needle.$this->brackets['post'];

			if(strpos($subject, $needle) !== false) {
				if(is_a($replace, 'Closure'))
					$subject = str_replace($needle, call_user_func($replace), $subject);
				else
					$subject = str_replace($needle, $replace, $subject);
			}
		}
		return $subject;
	}

}