<?php

include_once('PHPUnit/Autoload.php');

$loader = include_once(__DIR__.'/../vendor/autoload.php');

$loader->add('WSIServices\\Common', __DIR__.'/source');
