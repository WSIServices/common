<?php

namespace WSIServices\Common\Mock;

use WSIServices\Common\ArrayClass;

class ArrayClassMock extends ArrayClass {
	public $PublicProperty1 = 'value1';
	public $PublicProperty2 = 'value2';
	protected $ProtectedProperty1 = 'value3';
	protected $ProtectedProperty2 = 'value4';
	private $PrivateProperty1 = 'value5';
	private $PrivateProperty2 = 'value6';

	public function get_object_vars() {
		return get_object_vars($this);
	}
}
