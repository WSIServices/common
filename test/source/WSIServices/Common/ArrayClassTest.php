<?php

namespace WSIServices\Common;

require __DIR__.'/../../../../vendor/autoload.php';

use WSIServices\Common\Mock\ArrayClassMock;

/**
 * Test class for {@see ArrayClass}.
 * Generated on 2013-02-14.
 * 
 * @author Sam Likins
 * @copyright Copyright (c) 2013, WSI-Services
 * @license http://opensource.org/licenses/gpl-3.0.html
 * 
 * @covers WSIServices\Common\ArrayClass
 */
class ArrayClassTest extends \PHPUnit_Framework_TestCase {

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers WSIServices\Common\ArrayClass::__construct
	 */
	public function test__construct_String() {
		$configuration = 'test';
		$object = new ArrayClassMock($configuration);

		$object->PublicProperty2 = 'set';

		$this->assertSame(
			array(
				'PublicProperty1' => 'value1',
				'PublicProperty2' => 'set',
				'ProtectedProperty1' => 'value3',
				'ProtectedProperty2' => 'value4',
			), 
			$configuration,
			'Passed configuration variable not set to object values'
		);
	}

	/**
	 * @covers WSIServices\Common\ArrayClass::__construct
	 */
	public function test__construct_Null() {
		$configuration = null;
		$object = new ArrayClassMock($configuration);

		$object->PublicProperty2 = 'set';

		$this->assertSame(
			array(
				'PublicProperty1' => 'value1',
				'PublicProperty2' => 'set',
				'ProtectedProperty1' => 'value3',
				'ProtectedProperty2' => 'value4',
			), 
			$configuration,
			'Passed configuration variable not set to object values'
		);
	}

	/**
	 * @covers WSIServices\Common\ArrayClass::__construct
	 */
	public function test__construct_ArrayEmpty() {
		$configuration = array();
		$object = new ArrayClassMock($configuration);

		$object->PublicProperty2 = 'set';

		$this->assertSame(
			array(
				'PublicProperty1' => 'value1',
				'PublicProperty2' => 'set',
				'ProtectedProperty1' => 'value3',
				'ProtectedProperty2' => 'value4',
			), 
			$configuration,
			'Passed configuration variable not set to object values'
		);
	}
	
	/**
	 * @covers WSIServices\Common\ArrayClass::__construct
	 */
	public function test__construct_Array() {
		$configuration = array(
			'PublicProperty1' => 'works',
			'ProtectedProperty1' => 'works',
			'PrivateProperty1' => 'works',
			'nonExist' => false,
		);
		$object = new ArrayClassMock($configuration);

		$object->PublicProperty2 = 'set';

		$this->assertSame(
			array(
				'PublicProperty1' => 'works',
				'ProtectedProperty1' => 'works',
				'PrivateProperty1' => 'works',
				'nonExist' => false,
				'PublicProperty2' => 'set',
				'ProtectedProperty2' => 'value4',
			),
			$configuration,
			'Passed configuration variable not set to object values'
		);

		$this->assertSame(
			array(
				'PublicProperty1' => 'works',
				'PublicProperty2' => 'set',
				'ProtectedProperty1' => 'works',
				'ProtectedProperty2' => 'value4',
				'PrivateProperty1' => 'value5',
				'PrivateProperty2' => 'value6',
			),
			$object->get_object_vars(),
			'Passed configuration variable not set to object values'
		);
	}

}