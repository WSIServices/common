<?php

namespace WSIServices\Common\Configuration\Mock;

use WSIServices\Common\Configuration\NonReferenceTrait;

class NonReferenceMock extends ConfigurationContainerMock {
	use NonReferenceTrait;

}
