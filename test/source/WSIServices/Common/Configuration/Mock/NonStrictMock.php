<?php

namespace WSIServices\Common\Configuration\Mock;

use WSIServices\Common\Configuration\NonStrictTrait;

class NonStrictMock extends ConfigurationContainerMock {
	use NonStrictTrait {
		processInternalConfiguration as public;
	}

}
