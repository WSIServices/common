<?php

namespace WSIServices\Common\Configuration\Mock;

use WSIServices\Common\Configuration\ReferenceTrait;

class ReferenceMock extends ConfigurationContainerMock {
	use ReferenceTrait;

}
