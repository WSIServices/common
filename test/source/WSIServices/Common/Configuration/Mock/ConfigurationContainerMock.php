<?php

namespace WSIServices\Common\Configuration\Mock;

class ConfigurationContainerMock {

	public $PublicProperty1 = 'value1';
	public $PublicProperty2 = 'value2';
	protected $ProtectedProperty1 = 'value3';
	protected $ProtectedProperty2 = 'value4';
	private $PrivateProperty1 = 'value5';
	private $PrivateProperty2 = 'value6';

}
