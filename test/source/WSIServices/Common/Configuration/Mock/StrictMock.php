<?php

namespace WSIServices\Common\Configuration\Mock;

use WSIServices\Common\Configuration\StrictTrait;

class StrictMock extends ConfigurationContainerMock {
	use StrictTrait {
		processInternalConfiguration as public;
	}

}
