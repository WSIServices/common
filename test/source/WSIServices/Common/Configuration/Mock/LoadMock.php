<?php

namespace WSIServices\Common\Configuration\Mock;

use WSIServices\Common\Configuration\LoadTrait;

class LoadMock extends ConfigurationContainerMock {
	use LoadTrait;

}
