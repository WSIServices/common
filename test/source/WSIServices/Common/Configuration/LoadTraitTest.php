<?php

namespace WSIServices\Common\Configuration;

require __DIR__.'/../../../../../vendor/autoload.php';

use WSIServices\Common\Configuration\Mock\LoadMock;

/**
 * Test class for {@see NonReferenceTrait}.
 * Generated on 2013-03-11.
 * 
 * @author Sam Likins
 * @copyright Copyright (c) 2013, WSI-Services
 * @license http://opensource.org/licenses/gpl-3.0.html
 * 
 * @covers WSIServices\Common\Configuration\LoadTrait
 */
class LoadTraitTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var NonReferenceMock
	 */
	protected $configurationContainer;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->configurationContainer = $this->getMock(
			'WSIServices\Common\Configuration\Mock\LoadMock',
			array('processInternalConfiguration')
		);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers WSIServices\Common\Configuration\LoadTrait::loadConfiguration
	 * @expectedException UnexpectedValueException
	 */
	public function testLoadConfiguration_NonStringFileName() {
		$configurationFile = null;

		$this->configurationContainer
			->LoadConfiguration($configurationFile);
	}

	/**
	 * @covers WSIServices\Common\Configuration\LoadTrait::loadConfiguration
	 * @expectedException InvalidArgumentException
	 */
	public function testLoadConfiguration_NonExistConfiguration() {
		$configurationFile = __DIR__.'/LoadTraitTest/configuration.nonexist.config.php';

		$this->configurationContainer
			->LoadConfiguration($configurationFile);
	}

	/**
	 * @covers WSIServices\Common\Configuration\LoadTrait::loadConfiguration
	 * @expectedException UnexpectedValueException
	 */
	public function testLoadConfiguration_NonArrayConfiguration() {
		$configurationFile = __DIR__.'/LoadTraitTest/configuration.bad.config.php';

		$this->configurationContainer
			->LoadConfiguration($configurationFile);
	}

	/**
	 * @covers WSIServices\Common\Configuration\LoadTrait::loadConfiguration
	 */
	public function testLoadConfiguration() {
		$configurationFile = __DIR__.'/LoadTraitTest/configuration.config.php';

		$configuration = include($configurationFile);

		// Configure the stub
		$this->configurationContainer
			->expects($this->once())
			->method('processInternalConfiguration')
			->with($this->equalTo($configuration));

		$this->configurationContainer
			->LoadConfiguration($configurationFile);
	}

}