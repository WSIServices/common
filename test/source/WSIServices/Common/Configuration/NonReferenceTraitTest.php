<?php

namespace WSIServices\Common\Configuration;

require __DIR__.'/../../../../../vendor/autoload.php';

use WSIServices\Common\Configuration\Mock\NonReferenceMock;

/**
 * Test class for {@see NonReferenceTrait}.
 * Generated on 2013-03-11.
 * 
 * @author Sam Likins
 * @copyright Copyright (c) 2013, WSI-Services
 * @license http://opensource.org/licenses/gpl-3.0.html
 * 
 * @covers WSIServices\Common\Configuration\NonReferenceTrait
 */
class NonReferenceTraitTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var NonReferenceMock
	 */
	protected $configurationContainer;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->configurationContainer = $this->getMock(
			'WSIServices\Common\Configuration\Mock\NonReferenceMock',
			array('processInternalConfiguration')
		);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers WSIServices\Common\Configuration\ReferenceTrait::setConfiguration
	 */
	public function testSetConfiguration_Null() {
		$configuration = null;

		// Configure the stub
		$this->configurationContainer
			->expects($this->once())
			->method('processInternalConfiguration')
			->with($this->equalTo(array()));

		$this->configurationContainer
			->setConfiguration($configuration);

		$this->assertSame(null, $configuration);
	}

	/**
	 * @covers WSIServices\Common\Configuration\ReferenceTrait::setConfiguration
	 */
	public function testSetConfiguration_String() {
		$configuration = 'test';

		// Configure the stub
		$this->configurationContainer
			->expects($this->once())
			->method('processInternalConfiguration')
			->with($this->equalTo(array()));

		$this->configurationContainer
			->setConfiguration($configuration);

		$this->assertSame('test', $configuration);
	}

	/**
	 * @covers WSIServices\Common\Configuration\ReferenceTrait::setConfiguration
	 */
	public function testSetConfiguration_ArrayEmpty() {
		$configuration = array();

		// Configure the stub
		$this->configurationContainer
			->expects($this->once())
			->method('processInternalConfiguration')
			->with($this->equalTo($configuration));

		$this->configurationContainer
			->setConfiguration($configuration);

		$this->assertSame(array(), $configuration);
	}

	/**
	 * @covers WSIServices\Common\Configuration\ReferenceTrait::setConfiguration
	 */
	public function testSetConfiguration_Array() {
		$configuration = array(
			'test1' => 'value1',
			'test2' => 'value2',
			'test3' => 'value3',
		);

		// Configure the stub
		$this->configurationContainer
			->expects($this->once())
			->method('processInternalConfiguration')
			->with($this->equalTo($configuration));

		$this->configurationContainer
			->setConfiguration($configuration);

		$this->assertSame(
			array(
				'test1' => 'value1',
				'test2' => 'value2',
				'test3' => 'value3',
			),
			$configuration
		);
	}

}