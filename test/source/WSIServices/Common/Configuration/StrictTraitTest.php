<?php

namespace WSIServices\Common\Configuration;

require __DIR__.'/../../../../../vendor/autoload.php';

use WSIServices\Common\Configuration\Mock\StrictMock;

/**
 * Test class for {@see StrictTrait}.
 * Generated on 2013-03-11.
 * 
 * @author Sam Likins
 * @copyright Copyright (c) 2013, WSI-Services
 * @license http://opensource.org/licenses/gpl-3.0.html
 * 
 * @covers WSIServices\Common\Configuration\StrictTrait
 */
class StrictTraitTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var array 
	 */
	protected $configuration = array();

	/**
	 * @var StrictTraitMock
	 */
	protected $configurationContainer;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->configuration = array(
			'PublicProperty1' => 'works',
			'ProtectedProperty1' => 'works',
			'PrivateProperty1' => 'works',
			'nonExist' => true,
		);

		$this->configurationContainer = new StrictMock();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers WSIServices\Common\Configuration\StrictTrait::processInternalConfiguration
	 */
	public function testProcessInternalConfiguration_ArrayEmpty() {
		$configuration = array();
		$this->configurationContainer
			->processInternalConfiguration($configuration);

		$this->assertSame('value1', $this->configurationContainer->PublicProperty1);

		$this->configurationContainer->PublicProperty1 = 'test2';

		$this->assertSame(array(), $configuration);
	}

	/**
	 * @covers WSIServices\Common\Configuration\StrictTrait::processInternalConfiguration
	 */
	public function testProcessInternalConfiguration_Array() {
		$this->configurationContainer
			->processInternalConfiguration($this->configuration);

		$this->assertSame('works', $this->configurationContainer->PublicProperty1);
		$this->assertObjectNotHasAttribute('nonExist', $this->configurationContainer);

		$this->configurationContainer->PublicProperty1 = 'test2';

		$this->assertSame('test2', $this->configuration['PublicProperty1']);
	}

}